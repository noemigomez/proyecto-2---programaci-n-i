#!/usr/bin/env python3
# ! -*- coding:utf-8 -*-


#def imprimir(matriz):

def juego(players):
    matriz = []


def jugadores():
    try:
        print('El juego necesita entre 1 y 4 jugadores.')
        players = int(input('Ingrese cantidad de jugadores: '))
        if 1 <= players <= 4:
            juego(players)
            # print('holi')
        else:
            print('De 1 a 4 po, avíspate. De nuevo...')
            jugadores()
    except ValueError:
        print('Alta anarquía. ES UN NÚMERO POOOO. De nuevo')
        jugadores()


def instrucciones ():
    # aquí hay que hacer lo de archivo
    manual = open('instrucciones.txt')
    for linea in manual:
        print (linea)

def menu():
    try:
        print('1. Jugar\n2. Ver instructivo\n3. Salir')
        seleccion = int(input('Por favor, seleccione una opción: '))
        if seleccion == 1:
            jugadores()
            menu()
        elif seleccion == 2:
            instrucciones()
            menu()
        elif seleccion == 3:
            print('Gracias.')
    except ValueError:
        print('No seleccionaste nada del menú. Intente nuevamente')
        menu()

if __name__ == "__main__":
    """
        Función principal
    """
    print('BIENVENIDO A SCRABBLE')
    menu()
